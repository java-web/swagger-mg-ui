
/**
 * 使用服务器端请求的工具类
 */
var httpUtil = {
	get: function(param, success, fail, complete) {
		this.send(param, success, fail, complete);
	},
	post: function(param, success, fail, complete) {
		this.send(param, success, fail, complete);
	},
	send: function(param, success=function() {}, fail=function() {}, complete=function() {}) {
		ajaxTemp("swagger-mg-ui/http/request", "post", "json", param, function(result){
				success(result);
			}, function(msg){
				fail(result);
			}, function(xhr){
				complete(xhr);
			}
		);
	}
	
}

